﻿using UnityEngine;
using System.Collections;

public class Player_PhysicalMovement : MonoBehaviour {

	public Transform groundCheck;
	public LayerMask playerLayer;

	float m_xForce;
	float m_zForce;

	public static Vector3 m_forceVector;
	Quaternion m_rotationQuaternion;
	Transform m_cameraCenter;
	public Rigidbody m_playerRB;
	public float m_playerNormalSpeed;
	public float m_playerCurrentSpeed;
	public float m_playerRunningSpeed;

	public float m_jumpForce;
	public float m_maxSpeed;

	public bool m_grounded;
	public Vector3 m_clampedForceVector;


	[Header("Crouching")]
	public static bool isCrouching;
	public bool strgKey;

// Use this for initialization
	void Start () {

		m_cameraCenter = transform.GetChild (1);
		m_playerRB = GetComponent<Rigidbody> ();

		}
	
// Update is called once per frame
	void Update () {

//Crouching

		//mit STRG
		if (Input.GetKey (KeyCode.LeftControl) && strgKey == true && m_grounded) {
		
			isCrouching = true;

		} else if (strgKey == true){
		
			isCrouching = false;

		}

		//mit C & V
		if (Input.GetKeyDown (KeyCode.V) && m_grounded || Input.GetKeyDown (KeyCode.C) && m_grounded) {

			isCrouching = !isCrouching;
			strgKey = !strgKey;

		} 

//Sprint
		if (Input.GetKey (KeyCode.LeftShift)) {
			m_maxSpeed = Mathf.Lerp (m_maxSpeed, m_playerRunningSpeed, 0.2f);
		} else {
			m_maxSpeed = Mathf.Lerp (m_maxSpeed, m_playerNormalSpeed, 0.2f); 
		}

		m_xForce = Input.GetAxisRaw ("Horizontal") * m_playerCurrentSpeed * Time.deltaTime;
		m_zForce = Input.GetAxisRaw ("Vertical") * m_playerCurrentSpeed * Time.deltaTime;
//Rotation vom CameraCenter
		m_rotationQuaternion = Quaternion.Euler (0, m_cameraCenter.rotation.eulerAngles.y, 0);   

//Berechnung vom ForceVector mit Drehung
		m_forceVector = m_rotationQuaternion * new Vector3 (m_xForce, 0f, m_zForce);	

//Adding Force
		m_playerRB.AddForce (new Vector3 (m_forceVector.x, 0, m_forceVector.z), ForceMode.Impulse);
			
//Clamping the Force
		m_clampedForceVector = Vector3.ClampMagnitude (m_playerRB.velocity, m_maxSpeed);
		m_playerRB.velocity = new Vector3 (m_clampedForceVector.x, m_playerRB.velocity.y, m_clampedForceVector.z);


//Jump

		if (Input.GetKeyDown (KeyCode.Space)) {
			if (m_grounded) {
				m_playerRB.AddForce (new Vector3 (0, m_jumpForce, 0), ForceMode.Impulse);
			} 
		}

		m_grounded = Physics.CheckSphere (groundCheck.position, 0.20f, playerLayer);


		if(m_xForce == 0){
			m_playerRB.velocity = new Vector3 (Mathf.Lerp(m_playerRB.velocity.x, 0f, 0.1f), m_playerRB.velocity.y, m_playerRB.velocity.z);
		}
		
		if(m_zForce == 0){
			m_playerRB.velocity = new Vector3 (m_playerRB.velocity.x, m_playerRB.velocity.y, Mathf.Lerp(m_playerRB.velocity.z, 0f, 0.1f));
		}

	}

	void OnTriggerEnter(Collider col){
		m_grounded = true;
	}

	void OnTriggerExit(Collider col){
		m_grounded = false;
	}

}
