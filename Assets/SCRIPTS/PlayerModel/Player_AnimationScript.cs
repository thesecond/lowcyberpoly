﻿using UnityEngine;
using System.Collections;

public class Player_AnimationScript : MonoBehaviour {

	//Animator
	public Animator player_Animator;
	public Player_PhysicalMovement pc_Script;

	// Use this for initialization
	void Start () {
	
		player_Animator = GetComponent<Animator> ();
		pc_Script = transform.parent.GetComponent<Player_PhysicalMovement> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		player_Animator.SetBool ("isCrouching", Player_PhysicalMovement.isCrouching);
	}

	void OnTriggerEnter(Collider col){

		if (col.gameObject.tag == "Player") {
		
			Debug.Log ("NINININIIININO");
		}
	}
}
