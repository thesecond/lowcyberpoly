﻿using UnityEngine;
using System.Collections;

public class Player_CameraMovement : MonoBehaviour {

//Variabeln
	public float m_yMouseInput; 
	public float m_xMouseInput;
	public float m_mouseSensitivity;
	public float m_xRotation;
	public Transform m_rotationCenter;
	public float m_startXOffset;
	public float m_desiredXOffset;
	public float m_maxRayDistance;

	public CursorLockMode m_cursorLocked;

	[Header("Camera: Crouching")]
	public Vector3 crouchingCamPos;
	public Vector3 crouchingCamPosDown;
	public float crouchingValue;
	public bool stopCam = true;

	void Start(){

		//camera Pos wenn player isCrouching
		crouchingCamPos = transform.parent.transform.localPosition;
		crouchingCamPosDown = transform.parent.transform.localPosition;
		crouchingCamPosDown.y = crouchingCamPosDown.y - crouchingValue;

		m_rotationCenter = transform.parent;
		m_startXOffset = transform.localPosition.x;
		m_maxRayDistance = m_startXOffset * 4f;


		Cursor.lockState = m_cursorLocked;
	}


	void Update () {
	//Rotate Camera on Mousemovement
		RotateCamera ();
	//CameraCenter näher zum Player bringen, sobald er zu nahe an der Wand ist
		CameraRayCast ();
	//Camera Crouching anpassen
		CrouchingCamera();
	}


	void RotateCamera(){

	//Up-Down Rotation
		m_yMouseInput = Input.GetAxis ("Mouse Y")*m_mouseSensitivity;
		m_xRotation = m_xRotation - m_yMouseInput;
		m_xRotation = Mathf.Clamp(m_xRotation,-80,80);
	//transform.Rotate ändert Drehung UM bestimmten Wert
	//transform.rotation setzt Drehung AUF bestimmten Wert
		transform.localRotation = Quaternion.Euler (m_xRotation,transform.localRotation.eulerAngles.y,0);
	//Euler = Gradmaß, Quaternion = Bogenmaß
	//Left-Right Rotation
		m_xMouseInput = Input.GetAxis ("Mouse X")*m_mouseSensitivity;
		m_rotationCenter.Rotate(0,m_xMouseInput,0);
	}

	void CameraRayCast(){
	
	//neue variable vom Typ Ray
		Ray ray = new Ray (m_rotationCenter.position, m_rotationCenter.right);
	//neue Variable vom Typ RayCastHit
		RaycastHit hit = new RaycastHit ();
	//Falls der RayCast etwas trifft
		if(Physics.Raycast (ray, out hit,m_maxRayDistance)){
	//Berechne den neuen X Offset
			m_desiredXOffset = Mathf.Lerp (m_desiredXOffset, m_startXOffset - (m_maxRayDistance - hit.distance)/2.5f, 0.3f);
		}else{
	//Ansonsten geh wieder zurück zum ursprünglichen Offset
			m_desiredXOffset = Mathf.Lerp (m_desiredXOffset, m_startXOffset, 0.3f);
		}

		Debug.DrawLine (m_rotationCenter.position, hit.point);

		transform.localPosition = new Vector3 (m_desiredXOffset, 0, 0);

	}

	void CrouchingCamera(){

		if (Player_PhysicalMovement.isCrouching == true ) {

			transform.parent.transform.localPosition = Vector3.Lerp (transform.parent.transform.localPosition, crouchingCamPosDown, 0.1f);

		
		} else {
		
			transform.parent.transform.localPosition = Vector3.Lerp (transform.parent.transform.localPosition,crouchingCamPos, 0.1f);

		}


	}
}
