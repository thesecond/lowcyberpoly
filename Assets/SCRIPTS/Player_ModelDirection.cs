﻿using UnityEngine;
using System.Collections;

public class Player_ModelDirection : MonoBehaviour {

	public Transform m_facingArrow;
	public Transform m_directionTarget;


	// Use this for initialization
	void Start () {
	
		m_facingArrow = transform.parent.GetChild (2);
		m_directionTarget = transform.parent.GetChild (3);

	}
	
	// Update is called once per frame
	void Update () {
		
		CalculateRotation ();
		RotatePlayerModel ();
	}

	void CalculateRotation () {
		
		if (Player_PhysicalMovement.m_forceVector != Vector3.zero) {
			//DirectionTarget wird entsprechend der Force nach vorne/rechts/links ausgerichtet
			//.normalized reduziert die Länge des Vektors auf 1
			m_directionTarget.localPosition = Player_PhysicalMovement.m_forceVector.normalized;
//FacingArrow schaut zu DirectionTarget [Z-Achse]
			m_facingArrow.LookAt (m_directionTarget);
		}
	}

	void RotatePlayerModel (){
//ModelRotations Verzögerung [und Umsetzung]
		transform.localRotation = Quaternion.Lerp (transform.localRotation, m_facingArrow.localRotation,0.25f);
		}

}
	
