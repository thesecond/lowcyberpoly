﻿using UnityEngine;
using System.Collections;

public class Player_CameraCollision : MonoBehaviour {

	
	public float m_startDistance;
	public float m_desiredDistance;
	public Transform m_cameraCenter;
	/// <summary>
	/// The Offset the camera takes to the wall.
	/// </summary>
	public float m_wallOffset;

	// Use this for initialization
	void Start () {
		m_startDistance = -transform.localPosition.z;
		m_cameraCenter = transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
//Ray and RaycastHit
		Ray ray = new Ray (m_cameraCenter.position, transform.position - m_cameraCenter.position);
		RaycastHit hit = new RaycastHit ();
//Falls Ray etwas trifft
		if (Physics.Raycast (ray, out hit, m_startDistance+0.5f)) {
			
			m_wallOffset = Mathf.Lerp (m_wallOffset, hit.distance / 3.5f, 0.2f);

			if(hit.distance < m_startDistance){
				m_desiredDistance = hit.distance - m_wallOffset;
			}else{
				m_desiredDistance = m_startDistance - m_wallOffset;
			}

			Debug.DrawLine (m_cameraCenter.position, hit.point, Color.cyan);
		}else{
			m_wallOffset = Mathf.Lerp (m_wallOffset, 0, 0.1f);
			m_desiredDistance = m_startDistance - m_wallOffset;
		}

		transform.localPosition = new Vector3(0,0,-m_desiredDistance);
	
	}
}
